module CsvBuilder

  def build_content
    @csv_header = @response[0].keys
    @csv_body = @response.inject([]) { |arr, item| arr << item.values }
  end

  def to_csv
    CSV.open("csv_files/#{ @queries[@choice-1] }.csv", "wb") do |csv|
      csv << @csv_header
      @csv_body.each do |item|
        csv << item
      end
    end
  end

end
