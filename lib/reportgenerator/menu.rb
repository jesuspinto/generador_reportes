class Menu

  attr_accessor :choice, :response

  def self.show_menu
    system("clear")
    msg = "\n\nPlease choose one from the options below:\n"
    msg += "\n        1. Execute reports\n"
    msg += "\n        2. Create new reports\n"
    msg += "\n        3. Preferences\n"
    print msg
    loop do
      print "\nYour choice: "
      @choice = gets.chomp.to_i
      break if [1,2,3].include?( @choice )
      print "\n\nThat was an invalid choice. Please, try again\n\n"
    end
  end

  def self.sort
    case @choice
    when 1
      ReportBuilder.execute
    when 2
      puts "not yet"
    when 3
      puts "not yet"
    end
  end

  def self.execute
    show_menu
    sort
  end
end
